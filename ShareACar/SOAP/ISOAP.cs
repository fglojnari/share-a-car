﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.ServiceModel;
using System.Text;
using System.ServiceModel.Web;

namespace ShareACar
{
    [ServiceContract(Name = "SOAP", Namespace = "www.shareacar1.com/SOAP")]
    public interface ISOAP
    {

        //CARS---------------------------------------------------
        [OperationContract(Name = "createCar")]
        void createCar(string vehicleId, string name);

        [OperationContract(Name = "removeCar")]
        void removeCar(string vehicleId);


        [OperationContract(Name = "getCar")]
        Car getCar(string vehicleId);

        [OperationContract(Name = "getCars")]
        Car[] getCars();

        [OperationContract(Name = "getCarLocation")]
        Location getCarLocation(string vehicelId, string trassIpAddress);

        [OperationContract(Name = "findClosestCars")]
        Car[] findClosestCars(FindCarsRequest req);

        [OperationContract(Name = "carsNearBy", IsOneWay = true)]
        void carsNearBy(double range, double x, double y, string traasIpAdd);
        //CARS----------------------------------------------------------------------

        //CUSTOMERS------------------------------------------------------------------
        [OperationContract(Name = "createCustomer")]
        void createCustomer(int trr, string name, string surname);

        [OperationContract(Name = "removeCustomer")]
        void removeCustomer(int trr);

        [OperationContract(Name = "getCustomer")]
        Customer getCustomer(int trr);

        [OperationContract(Name = "getCustomers")]
        Customer[] getCustomers();

        [OperationContract(Name = "banCustomer")]
        void banCustomer(int trr, int days);
        //CUSTOMERS------------------------------------------------------------------

        //SESSIONS-------------------------------------------------------------------
        [OperationContract(Name = "startSharingSession")]
        void startSharingSession(string vehicleId, int trr, string ipAddr);

        [OperationContract(Name = "stopSharingSession")]
        void stopSharingSession(int sessionId, string ipAddr);

        [OperationContract(Name = "getSessions")]
        Session[] getSessions();

        [OperationContract(Name = "getSessionId")]
        int getSessionId(string vehicleId, string trr);

        [OperationContract(Name = "getSessionByVehIdTrr")]
        Session getSessionByVehIdTrr(string vehicleId, int trr);

    }

  
}
