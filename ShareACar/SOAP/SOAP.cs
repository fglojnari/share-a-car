﻿using System;
using System.Collections.Generic;
using System.ServiceModel;

namespace ShareACar
{
    // NOTE: You can use the "Rename" command on the "Refactor" menu to change the class name "Service1" in both code and config file together.
    public class SOAP : ISOAP
    {

        public void createCar(string vehicleId, string name)
        {
            try
            {
                //RegisterOBUService.CreateCar(serialNumber, name);
                Car car = new Car(vehicleId, name);
                BLL.createCar(car);
            }
            catch (FaultException e)
            {
                throw new FaultException(e.Message);
            }
        }

        public void removeCar(string vehicleId)
        {          
            try
            {
                BLL.removeCar(vehicleId);
            }
            catch (FaultException e)
            {
                throw new FaultException(e.Message);
            }
        }

        public Car getCar(string vehicleId)
        {        
            try
            {
                return BLL.getCar(vehicleId);
            }
            catch (FaultException e)
            {
                throw new FaultException(e.Message);
            }
        }

        public Car[] getCars()
        {           
            try
            {
                return BLL.getCars();
            }
            catch (FaultException e)
            {
                throw new FaultException(e.Message);
            }
        }

        public Location getCarLocation(string vehicelId, string trassIpAddress)
        {       
            try
            {
                return BLL.getCarLocation(vehicelId, trassIpAddress);
            }
            catch (FaultException e)
            {
                throw new FaultException(e.Message);
            }
        }
        public void carsNearBy(double range, double x, double y, string traasIpAdd)
        {
            try
            {
                Location l = new Location(x, y);
                BLL.carsNearBy(range, l, traasIpAdd);
            }
            catch
            {
                throw new Exception("Internal server error");
            }
        }

        public void createCustomer(int trr, string name, string surname)
        {
            try
            {
                Customer customer = new Customer(trr, name, surname);
                BLL.createCustomer(customer);
            }
            catch (FaultException e)
            {
                throw new FaultException(e.Message);
            }
        }

        public void removeCustomer(int trr)
        {
            try
            {
                BLL.removeCustomer(trr);
            }
            catch (FaultException e)
            {
                throw new FaultException(e.Message);
            }
        }

        public Customer getCustomer(int trr)
        {
            try
            {
                return BLL.getCustomer(trr);
            }
            catch (FaultException e)
            {
                throw new FaultException(e.Message);
            }  
        }

        public Customer[] getCustomers()
        {
            try
            {
                return BLL.getCustomers();
            }
            catch (FaultException e)
            {
                throw new FaultException(e.Message);
            }    
        }

        public void banCustomer(int trr, int days)
        {
            try
            {
                BLL.banCustomer(trr, days);
            }
            catch (FaultException e)
            {
                throw new FaultException(e.Message);
            }
        }

        public void startSharingSession(string vehicleId, int trr, string ipAddr)
        {
            try {
                BLL.startSharingSession(vehicleId, trr, ipAddr);
            }
            catch(FaultException e)
            {
                throw new FaultException(e.Message);
            }
        }

        public void stopSharingSession(int sessionId, string ipAddr)
        {
            try
            {
                BLL.stopSharingSession(sessionId, ipAddr);
            }
            catch (FaultException e)
            {
                throw new FaultException(e.Message);
            }
        }
   
        public Session getSessionByVehIdTrr(string vehicleId, int trr)
        {
            try
            {
                return BLL.getSessionByVehIdTrr(vehicleId, trr);
            }
            catch (FaultException e)
            {
                throw new FaultException(e.Message);
            }       
        }

        public int getSessionId(string vehicleId, string trr)
        {
            try
            {
                return BLL.getSessionId(vehicleId, int.Parse(trr));
            }
            catch (FaultException e)
            {
                throw new FaultException(e.Message);
            }
        }

        public Session[] getSessions()
        {
            try
            {
                return BLL.getSessions();
            }
            catch (FaultException e)
            {
                throw new FaultException(e.Message);
            }
        }

        public Car[] findClosestCars(FindCarsRequest req)
        {
            try
            {
                return BLL.findClosestCars(req.getRange(), req.getLocation().getLatitude(), req.getLocation().getLongitude(), req.getTraasIp());
            }
            catch (FaultException e)
            {
                throw new FaultException(e.Message);
            }
        }
    }
}
