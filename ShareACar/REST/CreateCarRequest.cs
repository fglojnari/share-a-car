﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.Text;
using System.Threading.Tasks;


namespace ShareACar
{
    [DataContract]
    public class CreateCarRequest
    {
        [DataMember]
        private string vehiclId;
        [DataMember]
        private string name;

        public CreateCarRequest(string vehiclId, string name) {
            this.vehiclId = vehiclId;
            this.name = name;
        }

        public string getName() {
            return name;
        }
        public string getVehicleId()
        {
            return vehiclId;
        }
    }
}
