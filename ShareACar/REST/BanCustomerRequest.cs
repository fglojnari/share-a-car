﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.Text;
using System.Threading.Tasks;

namespace ShareACar
{
    [DataContract]
    public class BanCustomerRequest
    {
        [DataMember]
        int trr;
        [DataMember]
        int days;
        public BanCustomerRequest(int trr, int days)
        {
            this.trr = trr;
            this.days = days;
        }
        public int getTrr() {
            return trr;
        }

        public int getDays()
        {
            return days;
        }
    }
}
