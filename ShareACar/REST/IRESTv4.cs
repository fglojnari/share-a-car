﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.ServiceModel;
using System.Text;
using System.ServiceModel.Web;

namespace ShareACar
{
    [ServiceContract(Name = "ShareACarServiceV4", Namespace = "www.shareacar1.com/V4")]
    public interface IRESTv4
    {
        [OperationContract(Name = "createCar")]
        [WebInvoke(Method = "POST", UriTemplate = "car", BodyStyle = WebMessageBodyStyle.Bare, RequestFormat = WebMessageFormat.Xml)]
        void createCar(CreateCarRequest request);

        [OperationContract(Name = "getCar")]
        [WebGet(UriTemplate = "car/{vehicleId}", BodyStyle = WebMessageBodyStyle.Bare, ResponseFormat = WebMessageFormat.Xml)]
        Car getCar(string vehicleId);

        [OperationContract(Name = "getCars")]
        [WebGet(UriTemplate = "car", BodyStyle = WebMessageBodyStyle.Bare, ResponseFormat = WebMessageFormat.Xml)]
        Car[] getCars();

        [OperationContract(Name = "updateCarName")]
        [WebInvoke(Method = "PUT", UriTemplate = "car", BodyStyle = WebMessageBodyStyle.Bare, RequestFormat = WebMessageFormat.Xml)]
        void updateCarName(CreateCarRequest carData);

        [OperationContract(Name = "removeCar")]
        [WebInvoke(Method = "DELETE", UriTemplate = "car/{vehicleId}", BodyStyle = WebMessageBodyStyle.Bare, ResponseFormat = WebMessageFormat.Xml)]
        void removeCar(string vehicleId);

        [OperationContract(Name = "getCarLocation")]
        [WebInvoke(Method = "POST", UriTemplate = "car/location", BodyStyle = WebMessageBodyStyle.Bare, ResponseFormat = WebMessageFormat.Xml)]
        Location getCarLocation(CarLocationRequest req);

        [OperationContract(Name = "findClosestCars")]
        [WebInvoke(Method = "POST", UriTemplate = "car/locations", BodyStyle = WebMessageBodyStyle.Bare, ResponseFormat = WebMessageFormat.Xml)]
        Car[] findClosestCars(FindCarsRequest req);

        [OperationContract(Name = "notifyImproperUse", IsOneWay = true)]
        [WebInvoke(Method = "POST", UriTemplate = "car/notifyimproper", BodyStyle = WebMessageBodyStyle.Bare, ResponseFormat = WebMessageFormat.Xml)]
        void notifyImproperUse(CarLocationRequest req);

        [OperationContract(Name = "carsNearBy", IsOneWay = true)]
        [WebInvoke(Method = "POST", UriTemplate = "car/nearby", BodyStyle = WebMessageBodyStyle.Bare, ResponseFormat = WebMessageFormat.Xml)]
        void carsNearBy(FindCarsRequest req);

        [OperationContract(Name = "stopCar")]
        [WebInvoke(Method = "GET", UriTemplate = "car/stop/{vehicleId}", BodyStyle = WebMessageBodyStyle.Bare, ResponseFormat = WebMessageFormat.Xml)]
        void stopCar(string vehicleId);

        [OperationContract(Name = "startSharingSession")]
        [WebInvoke(Method = "POST", UriTemplate = "session", BodyStyle = WebMessageBodyStyle.Bare, ResponseFormat = WebMessageFormat.Xml)]
        void startSharingSession(CreateSessionRequest sesRequest);


        [OperationContract(Name = "stopSharingSession")]
        [WebInvoke(Method = "PUT", UriTemplate = "session", BodyStyle = WebMessageBodyStyle.Bare, ResponseFormat = WebMessageFormat.Xml)]
        void stopSharingSession(StopSessionRequest req);


        [OperationContract(Name = "getSessions")]
        [WebGet(UriTemplate = "session", BodyStyle = WebMessageBodyStyle.Bare, ResponseFormat = WebMessageFormat.Xml)]
        Session[] getSessions();

        [OperationContract(Name = "getSessionById")]
        [WebGet(UriTemplate = "session/{id}", BodyStyle = WebMessageBodyStyle.Bare, ResponseFormat = WebMessageFormat.Xml)]
        Session getSessionById(string id);

        [OperationContract(Name = "getSessionId")]
        [WebGet(UriTemplate = "session/{vehicleId}/{trr}", BodyStyle = WebMessageBodyStyle.Bare, ResponseFormat = WebMessageFormat.Xml)]
        int getSessionId(string vehicleId, string trr);

        [OperationContract(Name = "removeSessionById")]
        [WebInvoke(Method = "DELETE", UriTemplate = "session/{id}", BodyStyle = WebMessageBodyStyle.Bare, ResponseFormat = WebMessageFormat.Xml)]
        void removeSessionById(string id);

        [OperationContract(Name = "createCustomer")]
        [WebInvoke(Method = "POST", UriTemplate = "customer", BodyStyle = WebMessageBodyStyle.Bare, ResponseFormat = WebMessageFormat.Xml)]
        void createCustomer(CreateCustomerRequest req);

        [OperationContract(Name = "getCustomer")]
        [WebGet(UriTemplate = "customer/{trr}", BodyStyle = WebMessageBodyStyle.Bare, ResponseFormat = WebMessageFormat.Xml)]
        Customer getCustomer(string trr);

        [OperationContract(Name = "getCustomers")]
        [WebGet(UriTemplate = "customer", BodyStyle = WebMessageBodyStyle.Bare, ResponseFormat = WebMessageFormat.Xml)]
        Customer[] getCustomers();

        [OperationContract(Name = "banCustomer")]
        [WebInvoke(Method = "PUT", UriTemplate = "customer", BodyStyle = WebMessageBodyStyle.Bare, ResponseFormat = WebMessageFormat.Xml)]
        void banCustomer(BanCustomerRequest req);

        [OperationContract(Name = "removeCustomer")]
        [WebInvoke(Method = "DELETE", UriTemplate = "customer/{trr}", BodyStyle = WebMessageBodyStyle.Bare, ResponseFormat = WebMessageFormat.Xml)]
        void removeCustomer(string trr);
    }
}
