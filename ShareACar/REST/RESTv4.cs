﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.ServiceModel;
using System.Text;
using System.IO;
using System.Xml.Serialization;
using System.Net;
using System.ServiceModel.Web;

namespace ShareACar
{
    public class RESTv4 : IRESTv4
    {

        public void createCar(CreateCarRequest request)
        {
            try
            {
                Car car = new Car(request.getVehicleId(), request.getName());
                BLL.createCar(car);
            }
            catch
            {
                throw new WebFaultException<string>("Wrong format", HttpStatusCode.BadRequest);
            }

        }
        public Car getCar(string vehicleId)
        {
            try
            {
                return BLL.getCar(vehicleId);
            }
            catch
            {
                throw new WebFaultException<string>("Wrong serial number", HttpStatusCode.BadRequest);
            }

        }

        public Car[] getCars()
        {
            try
            {
                return BLL.getCars();
            }
            catch (Exception e)
            {
                throw new WebFaultException<string>("Internal server error", HttpStatusCode.InternalServerError);
            }

        }
        public void updateCarName(CreateCarRequest carData)
        {
            try
            {
                BLL.updateCarName(carData.getVehicleId(), carData.getName());
            }
            catch
            {
                throw new WebFaultException<string>("Wrong format", HttpStatusCode.BadRequest);
            }
        }
        public void removeCar(string vehicleId)
        {
            try
            {
                BLL.removeCar(vehicleId);
            }
            catch
            {
                throw new WebFaultException<string>("Wrong serial number", HttpStatusCode.BadRequest);
            }

        }
        public Location getCarLocation(CarLocationRequest req)
        {
            try
            {
                return BLL.getCarLocation(req.getVehicleId(), req.getTraasIpAddr());
            }
            catch
            {
                throw new WebFaultException<string>("Internal server error", HttpStatusCode.InternalServerError);
            }
        }
        public Car[] findClosestCars(FindCarsRequest req)
        {
            try
            {
                return BLL.findClosestCars(req.getRange(), req.getLocation().getLatitude(), req.getLocation().getLongitude(), req.getTraasIp());
            }
            catch
            {
                throw new WebFaultException<string>("Internal server error", HttpStatusCode.InternalServerError);
            }
        }
        public void notifyImproperUse(CarLocationRequest req)
        {
            BLL.notifyImproperUse(req.getTraasIpAddr(), req.getVehicleId());
            try
            {
                BLL.notifyImproperUse(req.getTraasIpAddr(), req.getVehicleId());
            }
            catch
            {
                throw new WebFaultException<string>("Internal server error", HttpStatusCode.InternalServerError);
            }

        }
        public void carsNearBy(FindCarsRequest req)
        {
            try
            {
                BLL.carsNearBy(req.getRange(), req.getLocation(), req.getTraasIp());
            }
            catch
            {
                throw new WebFaultException<string>("Internal server error", HttpStatusCode.InternalServerError);
            }
        }
        public void stopCar(string vehicleId)
        {
            try
            {
                BLL.stopCar(vehicleId);
            }
            catch
            {
                throw new WebFaultException<string>("Internal server error", HttpStatusCode.InternalServerError);
            }
        }

        public void startSharingSession(CreateSessionRequest sesRequest)
        {
            try
            {
                BLL.startSharingSession(sesRequest.getVehicleId(), sesRequest.getTrr(), sesRequest.getTraasIpAddr());
            }
            catch
            {
                throw new WebFaultException<string>("There is no car that is avaiable with that id.", HttpStatusCode.BadRequest);
            }
        }

        public void stopSharingSession(StopSessionRequest req)
        {
            try
            {
                BLL.stopSharingSession(req.getSessionId(), req.getTraasIpAddr());
            }
            catch
            {
                throw new WebFaultException<string>("Internal server error.", HttpStatusCode.InternalServerError);
            }
        }

        public Session[] getSessions()
        {
            try
            {
                return BLL.getSessions();
            }
            catch
            {

                throw new WebFaultException<string>("Internal server error.", HttpStatusCode.InternalServerError);
            }
        }
        public int getSessionId(string vehicleId, string trr)
        {
            try
            {
                return BLL.getSessionId(vehicleId, int.Parse(trr));
            }
            catch
            {

                throw new WebFaultException<string>("Internal server error.", HttpStatusCode.InternalServerError);
            }
        }
        public Session getSessionById(string id)
        {
            try
            {
                return BLL.getSessionById(id);
            }
            catch
            {
                throw new WebFaultException<string>("Wrong id", HttpStatusCode.BadRequest);
            }
        }
        public void removeSessionById(string id)
        {
            try
            {
                BLL.removeSessionById(id);
            }
            catch
            {
                throw new WebFaultException<string>("Wrong id", HttpStatusCode.BadRequest);
            }
        }

        public void createCustomer(CreateCustomerRequest req)
        {
            try
            {
                Customer customer = new Customer(req.getTrr(), req.getName(), req.getSurname());
                BLL.createCustomer(customer);
                /*
                try { 
                    RegisterOBUService.CreateCar(serialNumber, name);  
                }
                catch(Exception e)
                {
                    return false;
                }  
                */
            }
            catch
            {
                throw new WebFaultException<string>("Wrong format.", HttpStatusCode.BadRequest);
            }

        }
        public Customer getCustomer(string trr)
        {
            try
            {
                return BLL.getCustomer(int.Parse(trr));
            }
            catch
            {
                throw new WebFaultException<string>("Wrong trr", HttpStatusCode.BadRequest);
            }

        }
        public Customer[] getCustomers()
        {
            try
            {
                return BLL.getCustomers();
            }
            catch
            {
                throw new WebFaultException<string>("Internal server error", HttpStatusCode.InternalServerError);
            }

        }
        public void banCustomer(BanCustomerRequest req)
        {
            try
            {
                BLL.banCustomer(req.getTrr(), req.getDays());
            }
            catch
            {
                throw new WebFaultException<string>("Wrong format", HttpStatusCode.BadRequest);
            }
        }
        public void removeCustomer(string trr)
        {
            try
            {
                BLL.removeCustomer(int.Parse(trr));
            }
            catch
            {
                throw new WebFaultException<string>("Wrong id", HttpStatusCode.BadRequest);
            }

        }
    }
}