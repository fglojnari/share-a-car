﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.Text;
using System.Threading.Tasks;

namespace ShareACar
{
    [DataContract]
    public class CreateCustomerRequest
    {
        [DataMember]
        int trr;
        [DataMember]
        string name;
        [DataMember]
        string surname;

        public CreateCustomerRequest(int trr, string name, string surname) {
            this.trr = trr;
            this.name = name;
            this.surname = surname;
        }

        public int getTrr() {
            return trr;
        }
        public string getName() {
            return name;
        }
        public string getSurname()
        {
            return surname;
        }
    }
}
