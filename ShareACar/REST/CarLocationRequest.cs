﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.Text;
using System.Threading.Tasks;

namespace ShareACar
{
    [DataContract]
    public class CarLocationRequest
    {
        [DataMember]
        private string vehicleId;
        [DataMember]
        private string traasIpAddr;

        public CarLocationRequest(string vehicleId, string ipAddr)
        {
            this.vehicleId = vehicleId;
            this.traasIpAddr = ipAddr;
        }
        public string getVehicleId()
        {
            return vehicleId;
        }
        public string getTraasIpAddr()
        {
            return traasIpAddr;
        }
    }
}
