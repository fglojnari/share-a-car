﻿namespace Client
{
    partial class Form1
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.menuStrip1 = new System.Windows.Forms.MenuStrip();
            this.carsToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.customersToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.sessionsToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.gbCustomerSettigns = new System.Windows.Forms.GroupBox();
            this.txtSurname = new System.Windows.Forms.TextBox();
            this.label5 = new System.Windows.Forms.Label();
            this.btnCreateCust = new System.Windows.Forms.Button();
            this.txtName = new System.Windows.Forms.TextBox();
            this.label3 = new System.Windows.Forms.Label();
            this.label4 = new System.Windows.Forms.Label();
            this.txtTRR = new System.Windows.Forms.TextBox();
            this.lbCustomers = new System.Windows.Forms.ListBox();
            this.gbCarSettings = new System.Windows.Forms.GroupBox();
            this.btnCreateCar = new System.Windows.Forms.Button();
            this.txtCarName = new System.Windows.Forms.TextBox();
            this.label2 = new System.Windows.Forms.Label();
            this.label1 = new System.Windows.Forms.Label();
            this.txtSerialNumber = new System.Windows.Forms.TextBox();
            this.lbCars = new System.Windows.Forms.ListBox();
            this.gbSessionsSettings = new System.Windows.Forms.GroupBox();
            this.groupBox1 = new System.Windows.Forms.GroupBox();
            this.lblEndTime = new System.Windows.Forms.Label();
            this.lblStartTime = new System.Windows.Forms.Label();
            this.btnStartSession = new System.Windows.Forms.Button();
            this.gbCustomer = new System.Windows.Forms.GroupBox();
            this.lblSurname = new System.Windows.Forms.Label();
            this.lblName = new System.Windows.Forms.Label();
            this.lblTRR = new System.Windows.Forms.Label();
            this.gbCar = new System.Windows.Forms.GroupBox();
            this.lblCarName = new System.Windows.Forms.Label();
            this.lblSerialNumber = new System.Windows.Forms.Label();
            this.lbSessions = new System.Windows.Forms.ListBox();
            this.btnUpdateDtb = new System.Windows.Forms.Button();
            this.menuStrip1.SuspendLayout();
            this.gbCustomerSettigns.SuspendLayout();
            this.gbCarSettings.SuspendLayout();
            this.gbSessionsSettings.SuspendLayout();
            this.groupBox1.SuspendLayout();
            this.gbCustomer.SuspendLayout();
            this.gbCar.SuspendLayout();
            this.SuspendLayout();
            // 
            // menuStrip1
            // 
            this.menuStrip1.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.carsToolStripMenuItem,
            this.customersToolStripMenuItem,
            this.sessionsToolStripMenuItem});
            this.menuStrip1.Location = new System.Drawing.Point(0, 0);
            this.menuStrip1.Name = "menuStrip1";
            this.menuStrip1.Size = new System.Drawing.Size(819, 24);
            this.menuStrip1.TabIndex = 0;
            this.menuStrip1.Text = "menuStrip1";
            // 
            // carsToolStripMenuItem
            // 
            this.carsToolStripMenuItem.Name = "carsToolStripMenuItem";
            this.carsToolStripMenuItem.Size = new System.Drawing.Size(42, 20);
            this.carsToolStripMenuItem.Text = "Cars";
            this.carsToolStripMenuItem.Click += new System.EventHandler(this.carsToolStripMenuItem_Click);
            // 
            // customersToolStripMenuItem
            // 
            this.customersToolStripMenuItem.Name = "customersToolStripMenuItem";
            this.customersToolStripMenuItem.Size = new System.Drawing.Size(76, 20);
            this.customersToolStripMenuItem.Text = "Customers";
            this.customersToolStripMenuItem.Click += new System.EventHandler(this.customersToolStripMenuItem_Click);
            // 
            // sessionsToolStripMenuItem
            // 
            this.sessionsToolStripMenuItem.Name = "sessionsToolStripMenuItem";
            this.sessionsToolStripMenuItem.Size = new System.Drawing.Size(63, 20);
            this.sessionsToolStripMenuItem.Text = "Sessions";
            this.sessionsToolStripMenuItem.Click += new System.EventHandler(this.sessionsToolStripMenuItem_Click);
            // 
            // gbCustomerSettigns
            // 
            this.gbCustomerSettigns.Controls.Add(this.txtSurname);
            this.gbCustomerSettigns.Controls.Add(this.label5);
            this.gbCustomerSettigns.Controls.Add(this.btnCreateCust);
            this.gbCustomerSettigns.Controls.Add(this.txtName);
            this.gbCustomerSettigns.Controls.Add(this.label3);
            this.gbCustomerSettigns.Controls.Add(this.label4);
            this.gbCustomerSettigns.Controls.Add(this.txtTRR);
            this.gbCustomerSettigns.Controls.Add(this.lbCustomers);
            this.gbCustomerSettigns.Location = new System.Drawing.Point(12, 27);
            this.gbCustomerSettigns.Name = "gbCustomerSettigns";
            this.gbCustomerSettigns.Size = new System.Drawing.Size(838, 426);
            this.gbCustomerSettigns.TabIndex = 1;
            this.gbCustomerSettigns.TabStop = false;
            this.gbCustomerSettigns.Text = "Customer Settings";
            // 
            // txtSurname
            // 
            this.txtSurname.Location = new System.Drawing.Point(159, 125);
            this.txtSurname.Name = "txtSurname";
            this.txtSurname.Size = new System.Drawing.Size(156, 20);
            this.txtSurname.TabIndex = 12;
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.Location = new System.Drawing.Point(159, 109);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(52, 13);
            this.label5.TabIndex = 11;
            this.label5.Text = "Surname:";
            // 
            // btnCreateCust
            // 
            this.btnCreateCust.Location = new System.Drawing.Point(159, 162);
            this.btnCreateCust.Name = "btnCreateCust";
            this.btnCreateCust.Size = new System.Drawing.Size(105, 23);
            this.btnCreateCust.TabIndex = 10;
            this.btnCreateCust.Text = "Create customer";
            this.btnCreateCust.UseVisualStyleBackColor = true;
            this.btnCreateCust.Click += new System.EventHandler(this.btnCreateCust_Click);
            // 
            // txtName
            // 
            this.txtName.Location = new System.Drawing.Point(159, 83);
            this.txtName.Name = "txtName";
            this.txtName.Size = new System.Drawing.Size(156, 20);
            this.txtName.TabIndex = 9;
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Location = new System.Drawing.Point(159, 67);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(38, 13);
            this.label3.TabIndex = 8;
            this.label3.Text = "Name:";
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Location = new System.Drawing.Point(160, 20);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(33, 13);
            this.label4.TabIndex = 7;
            this.label4.Text = "TRR:";
            // 
            // txtTRR
            // 
            this.txtTRR.Location = new System.Drawing.Point(159, 40);
            this.txtTRR.Name = "txtTRR";
            this.txtTRR.Size = new System.Drawing.Size(156, 20);
            this.txtTRR.TabIndex = 6;
            // 
            // lbCustomers
            // 
            this.lbCustomers.FormattingEnabled = true;
            this.lbCustomers.Location = new System.Drawing.Point(6, 19);
            this.lbCustomers.Name = "lbCustomers";
            this.lbCustomers.Size = new System.Drawing.Size(147, 394);
            this.lbCustomers.TabIndex = 0;
            // 
            // gbCarSettings
            // 
            this.gbCarSettings.Controls.Add(this.btnCreateCar);
            this.gbCarSettings.Controls.Add(this.txtCarName);
            this.gbCarSettings.Controls.Add(this.label2);
            this.gbCarSettings.Controls.Add(this.label1);
            this.gbCarSettings.Controls.Add(this.txtSerialNumber);
            this.gbCarSettings.Controls.Add(this.lbCars);
            this.gbCarSettings.Location = new System.Drawing.Point(12, 27);
            this.gbCarSettings.Name = "gbCarSettings";
            this.gbCarSettings.Size = new System.Drawing.Size(838, 426);
            this.gbCarSettings.TabIndex = 1;
            this.gbCarSettings.TabStop = false;
            this.gbCarSettings.Text = "Car Settings";
            this.gbCarSettings.Enter += new System.EventHandler(this.gbCarSettings_Enter);
            // 
            // btnCreateCar
            // 
            this.btnCreateCar.Location = new System.Drawing.Point(159, 122);
            this.btnCreateCar.Name = "btnCreateCar";
            this.btnCreateCar.Size = new System.Drawing.Size(75, 23);
            this.btnCreateCar.TabIndex = 5;
            this.btnCreateCar.Text = "Create car";
            this.btnCreateCar.UseVisualStyleBackColor = true;
            this.btnCreateCar.Click += new System.EventHandler(this.btnCreateCar_Click);
            // 
            // txtCarName
            // 
            this.txtCarName.Location = new System.Drawing.Point(159, 83);
            this.txtCarName.Name = "txtCarName";
            this.txtCarName.Size = new System.Drawing.Size(156, 20);
            this.txtCarName.TabIndex = 4;
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(159, 67);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(38, 13);
            this.label2.TabIndex = 3;
            this.label2.Text = "Name:";
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(160, 20);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(74, 13);
            this.label1.TabIndex = 2;
            this.label1.Text = "Serial number:";
            this.label1.Click += new System.EventHandler(this.label1_Click);
            // 
            // txtSerialNumber
            // 
            this.txtSerialNumber.Location = new System.Drawing.Point(159, 40);
            this.txtSerialNumber.Name = "txtSerialNumber";
            this.txtSerialNumber.Size = new System.Drawing.Size(156, 20);
            this.txtSerialNumber.TabIndex = 1;
            // 
            // lbCars
            // 
            this.lbCars.FormattingEnabled = true;
            this.lbCars.Location = new System.Drawing.Point(6, 19);
            this.lbCars.Name = "lbCars";
            this.lbCars.Size = new System.Drawing.Size(147, 394);
            this.lbCars.TabIndex = 0;
            // 
            // gbSessionsSettings
            // 
            this.gbSessionsSettings.Controls.Add(this.btnUpdateDtb);
            this.gbSessionsSettings.Controls.Add(this.groupBox1);
            this.gbSessionsSettings.Controls.Add(this.btnStartSession);
            this.gbSessionsSettings.Controls.Add(this.gbCustomer);
            this.gbSessionsSettings.Controls.Add(this.gbCar);
            this.gbSessionsSettings.Controls.Add(this.lbSessions);
            this.gbSessionsSettings.Location = new System.Drawing.Point(12, 27);
            this.gbSessionsSettings.Name = "gbSessionsSettings";
            this.gbSessionsSettings.Size = new System.Drawing.Size(1108, 509);
            this.gbSessionsSettings.TabIndex = 6;
            this.gbSessionsSettings.TabStop = false;
            this.gbSessionsSettings.Text = "Session Settings";
            // 
            // groupBox1
            // 
            this.groupBox1.Controls.Add(this.lblEndTime);
            this.groupBox1.Controls.Add(this.lblStartTime);
            this.groupBox1.Location = new System.Drawing.Point(203, 22);
            this.groupBox1.Name = "groupBox1";
            this.groupBox1.Size = new System.Drawing.Size(592, 129);
            this.groupBox1.TabIndex = 4;
            this.groupBox1.TabStop = false;
            this.groupBox1.Text = "Session";
            // 
            // lblEndTime
            // 
            this.lblEndTime.AutoSize = true;
            this.lblEndTime.Location = new System.Drawing.Point(19, 45);
            this.lblEndTime.Name = "lblEndTime";
            this.lblEndTime.Size = new System.Drawing.Size(35, 13);
            this.lblEndTime.TabIndex = 1;
            this.lblEndTime.Text = "label7";
            // 
            // lblStartTime
            // 
            this.lblStartTime.AutoSize = true;
            this.lblStartTime.Location = new System.Drawing.Point(19, 25);
            this.lblStartTime.Name = "lblStartTime";
            this.lblStartTime.Size = new System.Drawing.Size(35, 13);
            this.lblStartTime.TabIndex = 0;
            this.lblStartTime.Text = "label6";
            // 
            // btnStartSession
            // 
            this.btnStartSession.Location = new System.Drawing.Point(202, 321);
            this.btnStartSession.Name = "btnStartSession";
            this.btnStartSession.Size = new System.Drawing.Size(113, 23);
            this.btnStartSession.TabIndex = 3;
            this.btnStartSession.Text = "Start Session";
            this.btnStartSession.UseVisualStyleBackColor = true;
            this.btnStartSession.Click += new System.EventHandler(this.btnStartSession_Click);
            // 
            // gbCustomer
            // 
            this.gbCustomer.Controls.Add(this.lblSurname);
            this.gbCustomer.Controls.Add(this.lblName);
            this.gbCustomer.Controls.Add(this.lblTRR);
            this.gbCustomer.Location = new System.Drawing.Point(504, 157);
            this.gbCustomer.Name = "gbCustomer";
            this.gbCustomer.Size = new System.Drawing.Size(291, 129);
            this.gbCustomer.TabIndex = 2;
            this.gbCustomer.TabStop = false;
            this.gbCustomer.Text = "Customer";
            // 
            // lblSurname
            // 
            this.lblSurname.AutoSize = true;
            this.lblSurname.Location = new System.Drawing.Point(19, 74);
            this.lblSurname.Name = "lblSurname";
            this.lblSurname.Size = new System.Drawing.Size(35, 13);
            this.lblSurname.TabIndex = 2;
            this.lblSurname.Text = "label8";
            // 
            // lblName
            // 
            this.lblName.AutoSize = true;
            this.lblName.Location = new System.Drawing.Point(19, 51);
            this.lblName.Name = "lblName";
            this.lblName.Size = new System.Drawing.Size(35, 13);
            this.lblName.TabIndex = 1;
            this.lblName.Text = "label7";
            // 
            // lblTRR
            // 
            this.lblTRR.AutoSize = true;
            this.lblTRR.Location = new System.Drawing.Point(19, 30);
            this.lblTRR.Name = "lblTRR";
            this.lblTRR.Size = new System.Drawing.Size(35, 13);
            this.lblTRR.TabIndex = 0;
            this.lblTRR.Text = "label6";
            // 
            // gbCar
            // 
            this.gbCar.Controls.Add(this.lblCarName);
            this.gbCar.Controls.Add(this.lblSerialNumber);
            this.gbCar.Location = new System.Drawing.Point(202, 157);
            this.gbCar.Name = "gbCar";
            this.gbCar.Size = new System.Drawing.Size(291, 129);
            this.gbCar.TabIndex = 1;
            this.gbCar.TabStop = false;
            this.gbCar.Text = "Car";
            // 
            // lblCarName
            // 
            this.lblCarName.AutoSize = true;
            this.lblCarName.Location = new System.Drawing.Point(20, 51);
            this.lblCarName.Name = "lblCarName";
            this.lblCarName.Size = new System.Drawing.Size(35, 13);
            this.lblCarName.TabIndex = 1;
            this.lblCarName.Text = "label7";
            // 
            // lblSerialNumber
            // 
            this.lblSerialNumber.AutoSize = true;
            this.lblSerialNumber.Location = new System.Drawing.Point(20, 27);
            this.lblSerialNumber.Name = "lblSerialNumber";
            this.lblSerialNumber.Size = new System.Drawing.Size(35, 13);
            this.lblSerialNumber.TabIndex = 0;
            this.lblSerialNumber.Text = "label6";
            // 
            // lbSessions
            // 
            this.lbSessions.FormattingEnabled = true;
            this.lbSessions.Location = new System.Drawing.Point(6, 19);
            this.lbSessions.Name = "lbSessions";
            this.lbSessions.Size = new System.Drawing.Size(167, 394);
            this.lbSessions.TabIndex = 0;
            this.lbSessions.SelectedIndexChanged += new System.EventHandler(this.lbSessions_SelectedIndexChanged);
            // 
            // btnUpdateDtb
            // 
            this.btnUpdateDtb.Location = new System.Drawing.Point(467, 321);
            this.btnUpdateDtb.Name = "btnUpdateDtb";
            this.btnUpdateDtb.Size = new System.Drawing.Size(123, 23);
            this.btnUpdateDtb.TabIndex = 5;
            this.btnUpdateDtb.Text = "Update database";
            this.btnUpdateDtb.UseVisualStyleBackColor = true;
            this.btnUpdateDtb.Click += new System.EventHandler(this.button1_Click);
            // 
            // Form1
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(819, 465);
            this.Controls.Add(this.menuStrip1);
            this.Controls.Add(this.gbSessionsSettings);
            this.Controls.Add(this.gbCustomerSettigns);
            this.Controls.Add(this.gbCarSettings);
            this.MainMenuStrip = this.menuStrip1;
            this.Name = "Form1";
            this.Text = "ShareACar Administator";
            this.Load += new System.EventHandler(this.Form1_Load);
            this.menuStrip1.ResumeLayout(false);
            this.menuStrip1.PerformLayout();
            this.gbCustomerSettigns.ResumeLayout(false);
            this.gbCustomerSettigns.PerformLayout();
            this.gbCarSettings.ResumeLayout(false);
            this.gbCarSettings.PerformLayout();
            this.gbSessionsSettings.ResumeLayout(false);
            this.groupBox1.ResumeLayout(false);
            this.groupBox1.PerformLayout();
            this.gbCustomer.ResumeLayout(false);
            this.gbCustomer.PerformLayout();
            this.gbCar.ResumeLayout(false);
            this.gbCar.PerformLayout();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.MenuStrip menuStrip1;
        private System.Windows.Forms.ToolStripMenuItem carsToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem customersToolStripMenuItem;
        private System.Windows.Forms.GroupBox gbCustomerSettigns;
        private System.Windows.Forms.ListBox lbCustomers;
        private System.Windows.Forms.GroupBox gbCarSettings;
        private System.Windows.Forms.ListBox lbCars;
        private System.Windows.Forms.TextBox txtSerialNumber;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Button btnCreateCust;
        private System.Windows.Forms.TextBox txtName;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.TextBox txtTRR;
        private System.Windows.Forms.Button btnCreateCar;
        private System.Windows.Forms.TextBox txtCarName;
        private System.Windows.Forms.TextBox txtSurname;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.ToolStripMenuItem sessionsToolStripMenuItem;
        private System.Windows.Forms.GroupBox gbSessionsSettings;
        private System.Windows.Forms.ListBox lbSessions;
        private System.Windows.Forms.GroupBox gbCar;
        private System.Windows.Forms.Label lblCarName;
        private System.Windows.Forms.Label lblSerialNumber;
        private System.Windows.Forms.GroupBox gbCustomer;
        private System.Windows.Forms.Label lblSurname;
        private System.Windows.Forms.Label lblName;
        private System.Windows.Forms.Label lblTRR;
        private System.Windows.Forms.Button btnStartSession;
        private System.Windows.Forms.GroupBox groupBox1;
        private System.Windows.Forms.Label lblEndTime;
        private System.Windows.Forms.Label lblStartTime;
        private System.Windows.Forms.Button btnUpdateDtb;
    }
}

