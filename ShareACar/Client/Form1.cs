﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.Xml;
using Client.soap;

namespace Client
{
    public partial class Form1 : Form
    {
        //share1.share1PortTypeClient client;
        //share1.share1V2PortTypeClient client;
        //share1V3PortTypeClient client;
        soap.SOAPClient client;
        public Form1()
        {
            InitializeComponent();
            //client = new share1.share1PortTypeClient();
            //client = new share1V3PortTypeClient();
            client = new soap.SOAPClient();
            //localClient = new ServiceReference1.ShareACarService1Client();
        }

        private void Form1_Load(object sender, EventArgs e)
        {
            updateCarList();
            updateCustomerList();
            
        }

        private void updateCarList()
        {
            lbCars.Items.Clear();
            Car[] cars = client.getCars();
            foreach (Car car in cars)
                lbCars.Items.Add(car.vehicleId+"   "+car.name);
        }

        private void updateCustomerList()
        {
            lbCustomers.Items.Clear();
            Customer[] customers = client.getCustomers();
            foreach (Customer cust in customers)
                lbCustomers.Items.Add(cust.trr + "  " + cust.surname);
        }

        private void updateSessionList()
        {
            lbSessions.Items.Clear();
            Session[] sessions = client.getSessions();
            foreach (Session s in sessions)
                lbCustomers.Items.Add(s);

        }

        private void label1_Click(object sender, EventArgs e)
        {

        }

        private void btnCreateCar_Click(object sender, EventArgs e)
        {
            if(txtSerialNumber.Text != "" && txtCarName.Text != "")
            {
                //client.createCar(int.Parse(txtSerialNumber.Text), txtCarName.Text);
                //localClient.createCar(int.Parse(txtSerialNumber.Text), txtCarName.Text);
                updateCarList();
            }
        }

        private void gbCarSettings_Enter(object sender, EventArgs e)
        {

        }

        private void btnCreateCust_Click(object sender, EventArgs e)
        {
            if (txtTRR.Text != "" && txtName.Text != "" && txtSurname.Text != "")
            {
                client.createCustomer(int.Parse(txtTRR.Text), txtName.Text, txtSurname.Text);
                updateCustomerList();
            }
        }

        private void customersToolStripMenuItem_Click(object sender, EventArgs e)
        {
            gbCustomerSettigns.BringToFront();
        }

        private void carsToolStripMenuItem_Click(object sender, EventArgs e)
        {
            gbCarSettings.BringToFront();
        }

        private void sessionsToolStripMenuItem_Click(object sender, EventArgs e)
        {
            gbSessionsSettings.BringToFront();
        }

        private void btnStartSession_Click(object sender, EventArgs e)
        {
            /*
            if (lbCars.SelectedItem != null && lbCars.SelectedItem != null)
            {
                string[] carText = lbCars.GetItemText(lbCars.SelectedItem).Split(' ');
                Car car = client.getCarBySerialNumber(int.Parse(carText[0]));

                string[] custText = lbCustomers.GetItemText(lbCustomers.SelectedItem).Split(' ');
                Customer cust = client.getCustomerByTrr(int.Parse(custText[0]));

                client.startSharingSession(car.serialNumber, cust.trr);

                lbSessions.Items.Add((client.getSessionBySerNumbTrr(car.serialNumber, cust.trr)).car.serialNumber + " " + (client.getSessionBySerNumbTrr(car.serialNumber, cust.trr)).customer.trr);
            }*/
        }

        private void lbSessions_SelectedIndexChanged(object sender, EventArgs e)
        {
            /*
            string[] sessionText = lbSessions.GetItemText(lbSessions.SelectedItem).Split(' ');

            Car car = client.getCarBySerialNumber(int.Parse(sessionText[0]));
            Customer cust = client.getCustomerByTrr(int.Parse(sessionText[1]));
            Session sess = client.getSessionBySerNumbTrr(int.Parse(sessionText[0]), int.Parse(sessionText[1]));
            lblSerialNumber.Text = car.serialNumber.ToString();
            lblCarName.Text = car.name;
            lblTRR.Text = cust.trr.ToString();
            lblName.Text = cust.name;
            lblSurname.Text = cust.surname;
            lblStartTime.Text = sess.startTime.ToString();
            */
        }

        private void button1_Click(object sender, EventArgs e)
        {

            //share1.Customer cust = new share1.Customer();


            //XmlElement element = doc.CreateElement("Car")

            /*
            XmlNode node = doc.SelectSingleNode("/Database/Customers");
            XmlNode custNode = new XmlN
            node.AppendChild()*/
        }
    }
}
