﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Runtime.Serialization;

namespace ShareACar
{
    [DataContract]
    public class Car
    {
        [DataMember]
        string name = "";
        [DataMember]
        string vehicleId = "";

        public Car(string vehicleId, string name)
        {
            this.name = name;
            this.vehicleId = vehicleId;

        }

        public string getVehicleId()
        {
            return vehicleId;
        }
        public void setVehicleId(string vId)
        {
            vehicleId = vId;
        }
        public string getName()
        {
            return name;
        }


    }
}
