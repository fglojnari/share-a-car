﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.Text;
using System.Threading.Tasks;

namespace ShareACar
{
    [DataContract]
    public class FindCarsRequest
    {
        [DataMember]
        private double range;
        [DataMember]
        private Location location;
        [DataMember]
        private string traasIpAddr;
        public FindCarsRequest(double range, Location location, string traasIpAdd)
        {
            this.range = range;
            this.location = location;
            this.traasIpAddr = traasIpAdd;
        }

        public double getRange()
        {
            return range;
        }

        public Location getLocation()
        {
            return location;
        }
        public string getTraasIp()
        {
            return traasIpAddr;
        }
    }
}
