﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Xml;

namespace ShareACar
{
    public class Database
    {
        public static  void addCarToDatabase(Car car)
        {
            XmlDocument doc = new XmlDocument();
            doc.Load(@AppDomain.CurrentDomain.BaseDirectory + "/database.xml");
            XmlNode carsNode = doc.SelectSingleNode("/Database/Cars");

            XmlElement carElem= doc.CreateElement("Car");
            carElem.SetAttribute("vehicleId", car.getVehicleId());
            carElem.SetAttribute("carName", car.getName());
            carsNode.AppendChild(carElem);

            doc.Save(@AppDomain.CurrentDomain.BaseDirectory + "/database.xml");
        }


        public static Car[] getCars()
        {
            XmlDocument doc = new XmlDocument();
            doc.Load(@AppDomain.CurrentDomain.BaseDirectory + "/database.xml");
            XmlNodeList carNodeList = doc.SelectNodes("/Database/Cars/Car");

            Car[] carArray = new Car[carNodeList.Count];
            for (int i = 0; i < carNodeList.Count; i++)
            {
                carArray[i] = new Car(carNodeList[i].Attributes[0].Value, carNodeList[i].Attributes[1].Value);
            }
            return carArray;
        }

        public static void updateCarById(string vehicleId, string name)
        {
            XmlDocument doc = new XmlDocument();
            doc.Load(@AppDomain.CurrentDomain.BaseDirectory + "/database.xml");
            XmlNodeList carNodeList = doc.SelectNodes("/Database/Cars/Car");

            foreach (XmlNode node in carNodeList)
            {
                if (vehicleId == node.Attributes[0].Value)
                {
                    node.Attributes[1].Value = name;
                    doc.Save(@AppDomain.CurrentDomain.BaseDirectory + "/database.xml");
                    break;
                }
            }
        }

        public static void removeCarById(string vehicleId)
        {
            XmlDocument doc = new XmlDocument();
            doc.Load(@AppDomain.CurrentDomain.BaseDirectory + "/database.xml");
            XmlNodeList carList = doc.SelectNodes("/Database/Cars/Car");

            XmlNode carsNode = doc.SelectSingleNode("/Database/Cars");

            foreach (XmlNode node in carList)
            {
                if (vehicleId == node.Attributes[0].Value)
                {
                    carsNode.RemoveChild(node);
                    doc.Save(@AppDomain.CurrentDomain.BaseDirectory + "/database.xml");
                    break;
                }
            }
        }


        public static Car getCarById(string vehicleId)
        {
            XmlDocument doc = new XmlDocument();
            doc.Load(@AppDomain.CurrentDomain.BaseDirectory + "/database.xml");
            XmlNodeList custList = doc.SelectNodes("/Database/Cars/Car");

            List<Car> list = new List<Car>();
            foreach (XmlNode node in custList)
            {
                if (vehicleId == node.Attributes[0].Value)
                {
                    return new Car(vehicleId, node.Attributes[1].Value);
                }
            }
            return null;
        }

        public static void createSession(string vehicleId, int trr)
        {
            XmlDocument doc = new XmlDocument();
            doc.Load(@AppDomain.CurrentDomain.BaseDirectory + "/database.xml");
            XmlNode custNode = doc.SelectSingleNode("/Database/Sessions");

            int sessionId;
            XmlNodeList sessionNodeList = doc.SelectNodes("/Database/Sessions/Session");
            if (sessionNodeList.Count == 0)
            {
                sessionId = 1;
            }
            else {
                sessionId = sessionNodeList.Count + 1;
            }

            XmlElement cust = doc.CreateElement("Session");
            cust.SetAttribute("id", sessionId.ToString());
            cust.SetAttribute("vehicleId", vehicleId);
            cust.SetAttribute("trr", trr.ToString());
            cust.SetAttribute("startTime", DateTime.Now.ToString());
            cust.SetAttribute("endTime", null);
            custNode.AppendChild(cust);

            doc.Save(@AppDomain.CurrentDomain.BaseDirectory + "/database.xml");
        }

        public static void updateSession(int id)
        {
            XmlDocument doc = new XmlDocument();
            doc.Load(@AppDomain.CurrentDomain.BaseDirectory + "/database.xml");
            XmlNodeList sessionNodeList = doc.SelectNodes("/Database/Sessions/Session");

            foreach (XmlNode node in sessionNodeList)
            {
                int idAttr = int.Parse(node.Attributes[0].Value);
                if (id == idAttr)
                {
                    node.Attributes[4].Value = DateTime.Now.ToString();
                    doc.Save(@AppDomain.CurrentDomain.BaseDirectory + "/database.xml");
                    break;
                }
            }
        }

        public static void removeSessionById(int id)
        {
            XmlDocument doc = new XmlDocument();
            doc.Load(@AppDomain.CurrentDomain.BaseDirectory + "/database.xml");
            XmlNodeList sessionList = doc.SelectNodes("/Database/Sessions/Session");
            XmlNode sessionsNode = doc.SelectSingleNode("/Database/Sessions");

            foreach (XmlNode node in sessionList)
            {
                int idP = int.Parse(node.Attributes[0].Value);
                if (id == idP)
                {
                    sessionsNode.RemoveChild(node);
                    doc.Save(@AppDomain.CurrentDomain.BaseDirectory + "/database.xml");
                    break;
                }
            }
        }

        public static Session[] getSessions()
        {
            XmlDocument doc = new XmlDocument();
            doc.Load(@AppDomain.CurrentDomain.BaseDirectory + "/database.xml");
            XmlNodeList sessionNodeList = doc.SelectNodes("/Database/Sessions/Session");

            Session[] sessionArray = new Session[sessionNodeList.Count];
            for (int i = 0; i < sessionNodeList.Count; i++)
            {
                try
                {
                    DateTime endTime = Convert.ToDateTime(sessionNodeList[i].Attributes[4].Value);
                    sessionArray[i] = new Session(int.Parse(sessionNodeList[i].Attributes[0].Value), sessionNodeList[i].Attributes[1].Value, int.Parse(sessionNodeList[i].Attributes[2].Value), Convert.ToDateTime(sessionNodeList[i].Attributes[3].Value), endTime);
                }
                catch
                {
                    sessionArray[i] = new Session(int.Parse(sessionNodeList[i].Attributes[0].Value), sessionNodeList[i].Attributes[1].Value, int.Parse(sessionNodeList[i].Attributes[2].Value), Convert.ToDateTime(sessionNodeList[i].Attributes[3].Value, null));
                }

            }
            return sessionArray;
        }
        public static int getSessionId(string vehicleId, int trr)
        {
            XmlDocument doc = new XmlDocument();
            doc.Load(@AppDomain.CurrentDomain.BaseDirectory + "/database.xml");
            XmlNodeList sessList = doc.SelectNodes("/Database/Sessions/Session");

            foreach (XmlNode node in sessList)
            {
                if (vehicleId == node.Attributes[1].Value && trr == int.Parse(node.Attributes[2].Value))
                {
                    DateTime? temp;
                    try
                    {
                        temp = Convert.ToDateTime(node.Attributes[4].Value);
                    }
                    catch
                    {
                        temp = null;
                    }
                    return int.Parse(node.Attributes[0].Value);
                }
            }
            return -1;
        }
        public static Session getSessionById(int id)
        {
            XmlDocument doc = new XmlDocument();
            doc.Load(@AppDomain.CurrentDomain.BaseDirectory + "/database.xml");
            XmlNodeList custList = doc.SelectNodes("/Database/Sessions/Session");

            foreach (XmlNode node in custList)
            {
                int idP = int.Parse(node.Attributes[0].Value);
                if (id == idP)
                {
                    DateTime? temp;
                    try
                    {
                        temp = Convert.ToDateTime(node.Attributes[4].Value);
                    }
                    catch
                    {
                        temp = null;
                    }
                    return new Session(int.Parse(node.Attributes[0].Value), node.Attributes[1].Value, int.Parse(node.Attributes[2].Value), Convert.ToDateTime(node.Attributes[3].Value), temp);
                }
            }
            return null;
        }

        public static Session getSessionByVehIdTrr(string vehicleId, int trr)
        {
            XmlDocument doc = new XmlDocument();
            doc.Load(@AppDomain.CurrentDomain.BaseDirectory + "/database.xml");
            XmlNodeList sessList = doc.SelectNodes("/Database/Sessions/Session");

            foreach (XmlNode node in sessList)
            {
                if(vehicleId == node.Attributes[1].Value && trr == int.Parse(node.Attributes[2].Value))
                {
                    DateTime? temp;
                    try
                    {
                        temp = Convert.ToDateTime(node.Attributes[4].Value);
                    }
                    catch
                    {
                        temp = null;
                    }
                    return new Session(int.Parse(node.Attributes[0].Value), node.Attributes[1].Value, int.Parse(node.Attributes[2].Value), Convert.ToDateTime(node.Attributes[3].Value), temp);
                }
            }
            return null;
        }

        public static void addCustomerToDatabase(Customer customer)
        {
            XmlDocument doc = new XmlDocument();
            doc.Load(@AppDomain.CurrentDomain.BaseDirectory + "/database.xml");
            XmlNode custNode = doc.SelectSingleNode("/Database/Customers");

            XmlElement cust = doc.CreateElement("Customer");
            cust.SetAttribute("trr", customer.getTrr().ToString());
            cust.SetAttribute("name", customer.getName());
            cust.SetAttribute("surname", customer.getSurname());
            cust.SetAttribute("isBanned", "false");
            custNode.AppendChild(cust);

            doc.Save(@AppDomain.CurrentDomain.BaseDirectory + "/database.xml");
        }

        public static Customer[] getCustomers()
        {
            XmlDocument doc = new XmlDocument();
            doc.Load(@AppDomain.CurrentDomain.BaseDirectory + "/database.xml");
            XmlNodeList custNodeList = doc.SelectNodes("/Database/Customers/Customer");

            Customer[] custArray = new Customer[custNodeList.Count];
            for (int i = 0; i < custNodeList.Count; i++)
            {
                custArray[i] = new Customer(int.Parse(custNodeList[i].Attributes[0].Value), custNodeList[i].Attributes[1].Value, custNodeList[i].Attributes[2].Value);
            }
            return custArray;
        }

        public static void removeCustomerByTrr(int trr)
        {
            XmlDocument doc = new XmlDocument();
            doc.Load(@AppDomain.CurrentDomain.BaseDirectory + "/database.xml");
            XmlNodeList custList = doc.SelectNodes("/Database/Customers/Customer");

            XmlNode customersNode = doc.SelectSingleNode("/Database/Customers");

            foreach (XmlNode node in custList)
            {
                if (trr == int.Parse(node.Attributes[0].Value))
                {
                    customersNode.RemoveChild(node);
                    doc.Save(@AppDomain.CurrentDomain.BaseDirectory + "/database.xml");
                    break;
                }
            }
        }

        public static void banCustomer(int trr)
        {
            XmlDocument doc = new XmlDocument();
            doc.Load(@AppDomain.CurrentDomain.BaseDirectory + "/database.xml");
            XmlNodeList customersNodeList = doc.SelectNodes("/Database/Customers/Customer");

            foreach (XmlNode node in customersNodeList)
            {
                if (trr == int.Parse(node.Attributes[0].Value))
                {
                    node.Attributes[3].Value = "true";
                    doc.Save(@AppDomain.CurrentDomain.BaseDirectory + "/database.xml");
                    break;
                }
            }
        }

        public static Customer getCustomerByTrr(int trr)
        {
            XmlDocument doc = new XmlDocument();
            doc.Load(@AppDomain.CurrentDomain.BaseDirectory + "/database.xml");
            XmlNodeList custList = doc.SelectNodes("/Database/Customers/Customer");

            foreach (XmlNode node in custList)
            {
                int trrAtr = int.Parse(node.Attributes[0].Value);
                if (trr == trrAtr)
                {
                    return new Customer(trrAtr, node.Attributes[1].Value, node.Attributes[2].Value);
                }
            }
            return null;
        }
    }
}
