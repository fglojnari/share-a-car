﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Runtime.Serialization;
using System.ServiceModel;

namespace ShareACar
{
    [DataContract]
    public class Session
    {
        int id;
        [DataMember]
        string vehicleId;
        [DataMember]
        int trr;
        [DataMember]
        DateTime startTime;
        [DataMember]
        DateTime? endTime;
        
        public Session(int id, string vehicleId, int trr, DateTime startTime, DateTime? endTime = null)
        {
            this.id = id;
            this.vehicleId = vehicleId ;
            this.trr = trr;
            this.startTime = startTime;
            this.endTime = endTime;
        }

        public void setEndTime(DateTime endTime)
        {
            this.endTime = endTime;
        }

        public int getId()
        {
            return id;
        }

        public int getTrr()
        {
            return trr;
        }

        public string getVehicleId()
        {
            return vehicleId;
        }

        public DateTime getStartTime()
        {
            return startTime;
        }

        public DateTime getEndTime()
        {
            try
            {
                return (DateTime)endTime;
            }
            catch (Exception e)
            {
                throw new Exception(e.Message);
            }
        }

    }
}
