﻿using RabbitMQ.Client;
using RabbitMQ.Client.Events;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ShareACar
{
    public class Subscriber
    {
        public void GetMessages(string rabbitQName, string vehicleId)
        {
            ConnectionFactory factory = new ConnectionFactory();
            IProtocol protocol = Protocols.AMQP_0_9_1;
            factory.VirtualHost = "/";
            factory.UserName = "soa";
            factory.Password = "soasoa";
            factory.HostName = "164.8.251.96";
            factory.Port = 5672;
            factory.Protocol = protocol;

            using (var connection = factory.CreateConnection())
            using (var channel = connection.CreateModel())
            {

                channel.ExchangeDeclare(rabbitQName, "fanout", true);
                var queueName = channel.QueueDeclare().QueueName;
                channel.QueueBind(queueName, rabbitQName, "");

                var consumer = new EventingBasicConsumer(channel);

                System.Threading.Thread.Sleep(11000);
                consumer.Received += (model, ea) =>
                {
                    var body = ea.Body;
                    var message = Encoding.UTF8.GetString(body);
                    double speed = double.Parse(message);
                    if (speed > 50)
                    {
                        Logic.OBU1.obu1v3PortTypeClient obu1 = new Logic.OBU1.obu1v3PortTypeClient();
                        obu1.OBU1_SendMessage("http://164.8.251.96:8088/TRAAS_WS", vehicleId, "Vozite prehitro!");
                    }
                };
                channel.BasicConsume(queueName, noAck: true, consumer: consumer);
                System.Threading.Thread.Sleep(20000);
            }
        }
    }
}
