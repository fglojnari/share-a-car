﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Runtime.Serialization;

namespace ShareACar
{
    [DataContract]
    public class Customer
    {
        [DataMember]
        int trr = 0;
        [DataMember]
        string name = "";
        [DataMember]
        string surname = "";
        [DataMember]
        DateTime banDateEnd;
        [DataMember]
        bool banned;

        public Customer(int trr, string name, string surname)
        {
            this.trr = trr;
            this.name = name;
            this.surname = surname;
            banned = false;
        }

        public int getTrr()
        {
            return trr;
        }

        public string getName()
        {
            return name;
        }

        public string getSurname()
        {
            return surname;
        }

        public bool isBanned()
        {
            return banned;
        }

        public void ban()
        {
            banned = true;
        }

        public void setBanDateEnd(DateTime banDateEnd)
        {
            this.banDateEnd = banDateEnd;
        }

        public DateTime getBanDateEnd()
        {
            return banDateEnd;
        }
    }
}
