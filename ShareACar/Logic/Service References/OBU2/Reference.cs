﻿//------------------------------------------------------------------------------
// <auto-generated>
//     This code was generated by a tool.
//     Runtime Version:4.0.30319.42000
//
//     Changes to this file may cause incorrect behavior and will be lost if
//     the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------

namespace Logic.OBU2 {
    using System.Runtime.Serialization;
    using System;
    
    
    [System.Diagnostics.DebuggerStepThroughAttribute()]
    [System.CodeDom.Compiler.GeneratedCodeAttribute("System.Runtime.Serialization", "4.0.0.0")]
    [System.Runtime.Serialization.DataContractAttribute(Name="VhodniParametri", Namespace="http://schemas.datacontract.org/2004/07/registerOBU2_v03.DataContracts")]
    [System.SerializableAttribute()]
    public partial class VhodniParametri : object, System.Runtime.Serialization.IExtensibleDataObject, System.ComponentModel.INotifyPropertyChanged {
        
        [System.NonSerializedAttribute()]
        private System.Runtime.Serialization.ExtensionDataObject extensionDataField;
        
        [System.Runtime.Serialization.OptionalFieldAttribute()]
        private string KeyField;
        
        [System.Runtime.Serialization.OptionalFieldAttribute()]
        private string PotDoTraasaField;
        
        [global::System.ComponentModel.BrowsableAttribute(false)]
        public System.Runtime.Serialization.ExtensionDataObject ExtensionData {
            get {
                return this.extensionDataField;
            }
            set {
                this.extensionDataField = value;
            }
        }
        
        [System.Runtime.Serialization.DataMemberAttribute()]
        public string Key {
            get {
                return this.KeyField;
            }
            set {
                if ((object.ReferenceEquals(this.KeyField, value) != true)) {
                    this.KeyField = value;
                    this.RaisePropertyChanged("Key");
                }
            }
        }
        
        [System.Runtime.Serialization.DataMemberAttribute()]
        public string PotDoTraasa {
            get {
                return this.PotDoTraasaField;
            }
            set {
                if ((object.ReferenceEquals(this.PotDoTraasaField, value) != true)) {
                    this.PotDoTraasaField = value;
                    this.RaisePropertyChanged("PotDoTraasa");
                }
            }
        }
        
        public event System.ComponentModel.PropertyChangedEventHandler PropertyChanged;
        
        protected void RaisePropertyChanged(string propertyName) {
            System.ComponentModel.PropertyChangedEventHandler propertyChanged = this.PropertyChanged;
            if ((propertyChanged != null)) {
                propertyChanged(this, new System.ComponentModel.PropertyChangedEventArgs(propertyName));
            }
        }
    }
    
    [System.Diagnostics.DebuggerStepThroughAttribute()]
    [System.CodeDom.Compiler.GeneratedCodeAttribute("System.Runtime.Serialization", "4.0.0.0")]
    [System.Runtime.Serialization.DataContractAttribute(Name="Vehicle", Namespace="http://schemas.datacontract.org/2004/07/registerOBU2_v03.DataContracts")]
    [System.SerializableAttribute()]
    public partial class Vehicle : object, System.Runtime.Serialization.IExtensibleDataObject, System.ComponentModel.INotifyPropertyChanged {
        
        [System.NonSerializedAttribute()]
        private System.Runtime.Serialization.ExtensionDataObject extensionDataField;
        
        [global::System.ComponentModel.BrowsableAttribute(false)]
        public System.Runtime.Serialization.ExtensionDataObject ExtensionData {
            get {
                return this.extensionDataField;
            }
            set {
                this.extensionDataField = value;
            }
        }
        
        public event System.ComponentModel.PropertyChangedEventHandler PropertyChanged;
        
        protected void RaisePropertyChanged(string propertyName) {
            System.ComponentModel.PropertyChangedEventHandler propertyChanged = this.PropertyChanged;
            if ((propertyChanged != null)) {
                propertyChanged(this, new System.ComponentModel.PropertyChangedEventArgs(propertyName));
            }
        }
    }
    
    [System.CodeDom.Compiler.GeneratedCodeAttribute("System.ServiceModel", "4.0.0.0")]
    [System.ServiceModel.ServiceContractAttribute(Namespace="registerOBU2", ConfigurationName="OBU2.registerOBU2v03")]
    public interface registerOBU2v03 {
        
        [System.ServiceModel.OperationContractAttribute(Action="registerOBU2/registerOBU2v03/registerOBU2_AddCar", ReplyAction="registerOBU2/registerOBU2v03/registerOBU2_AddCarResponse")]
        [System.ServiceModel.FaultContractAttribute(typeof(System.ServiceModel.FaultException), Action="registerOBU2/registerOBU2v03/registerOBU2_AddCarFaultExceptionFault", Name="FaultException", Namespace="http://schemas.datacontract.org/2004/07/System.ServiceModel")]
        string registerOBU2_AddCar(Logic.OBU2.VhodniParametri VP, string tip_Vozila, string ID_pot, string trr_lastnika);
        
        [System.ServiceModel.OperationContractAttribute(Action="registerOBU2/registerOBU2v03/registerOBU2_AddCar", ReplyAction="registerOBU2/registerOBU2v03/registerOBU2_AddCarResponse")]
        System.Threading.Tasks.Task<string> registerOBU2_AddCarAsync(Logic.OBU2.VhodniParametri VP, string tip_Vozila, string ID_pot, string trr_lastnika);
        
        [System.ServiceModel.OperationContractAttribute(Action="registerOBU2/registerOBU2v03/registerOBU2_RemoveObsoleteCar", ReplyAction="registerOBU2/registerOBU2v03/registerOBU2_RemoveObsoleteCarResponse")]
        [System.ServiceModel.FaultContractAttribute(typeof(System.ServiceModel.FaultException), Action="registerOBU2/registerOBU2v03/registerOBU2_RemoveObsoleteCarFaultExceptionFault", Name="FaultException", Namespace="http://schemas.datacontract.org/2004/07/System.ServiceModel")]
        void registerOBU2_RemoveObsoleteCar(Logic.OBU2.VhodniParametri VP, string sifra_vozila);
        
        [System.ServiceModel.OperationContractAttribute(Action="registerOBU2/registerOBU2v03/registerOBU2_RemoveObsoleteCar", ReplyAction="registerOBU2/registerOBU2v03/registerOBU2_RemoveObsoleteCarResponse")]
        System.Threading.Tasks.Task registerOBU2_RemoveObsoleteCarAsync(Logic.OBU2.VhodniParametri VP, string sifra_vozila);
        
        [System.ServiceModel.OperationContractAttribute(Action="registerOBU2/registerOBU2v03/registerOBU2_CheckCarExsistance", ReplyAction="registerOBU2/registerOBU2v03/registerOBU2_CheckCarExsistanceResponse")]
        [System.ServiceModel.FaultContractAttribute(typeof(System.ServiceModel.FaultException), Action="registerOBU2/registerOBU2v03/registerOBU2_CheckCarExsistanceFaultExceptionFault", Name="FaultException", Namespace="http://schemas.datacontract.org/2004/07/System.ServiceModel")]
        bool registerOBU2_CheckCarExsistance(Logic.OBU2.VhodniParametri VP, string sifra_vozila);
        
        [System.ServiceModel.OperationContractAttribute(Action="registerOBU2/registerOBU2v03/registerOBU2_CheckCarExsistance", ReplyAction="registerOBU2/registerOBU2v03/registerOBU2_CheckCarExsistanceResponse")]
        System.Threading.Tasks.Task<bool> registerOBU2_CheckCarExsistanceAsync(Logic.OBU2.VhodniParametri VP, string sifra_vozila);
        
        [System.ServiceModel.OperationContractAttribute(Action="registerOBU2/registerOBU2v03/registerOBU2_GetCarEdge", ReplyAction="registerOBU2/registerOBU2v03/registerOBU2_GetCarEdgeResponse")]
        [System.ServiceModel.FaultContractAttribute(typeof(System.ServiceModel.FaultException), Action="registerOBU2/registerOBU2v03/registerOBU2_GetCarEdgeFaultExceptionFault", Name="FaultException", Namespace="http://schemas.datacontract.org/2004/07/System.ServiceModel")]
        string registerOBU2_GetCarEdge(Logic.OBU2.VhodniParametri VP, string sifra_vozila);
        
        [System.ServiceModel.OperationContractAttribute(Action="registerOBU2/registerOBU2v03/registerOBU2_GetCarEdge", ReplyAction="registerOBU2/registerOBU2v03/registerOBU2_GetCarEdgeResponse")]
        System.Threading.Tasks.Task<string> registerOBU2_GetCarEdgeAsync(Logic.OBU2.VhodniParametri VP, string sifra_vozila);
        
        [System.ServiceModel.OperationContractAttribute(Action="registerOBU2/registerOBU2v03/registerOBU2_GetPersonInCar", ReplyAction="registerOBU2/registerOBU2v03/registerOBU2_GetPersonInCarResponse")]
        [System.ServiceModel.FaultContractAttribute(typeof(System.ServiceModel.FaultException), Action="registerOBU2/registerOBU2v03/registerOBU2_GetPersonInCarFaultExceptionFault", Name="FaultException", Namespace="http://schemas.datacontract.org/2004/07/System.ServiceModel")]
        string registerOBU2_GetPersonInCar(Logic.OBU2.VhodniParametri VP, string sifra_vozila);
        
        [System.ServiceModel.OperationContractAttribute(Action="registerOBU2/registerOBU2v03/registerOBU2_GetPersonInCar", ReplyAction="registerOBU2/registerOBU2v03/registerOBU2_GetPersonInCarResponse")]
        System.Threading.Tasks.Task<string> registerOBU2_GetPersonInCarAsync(Logic.OBU2.VhodniParametri VP, string sifra_vozila);
        
        [System.ServiceModel.OperationContractAttribute(Action="registerOBU2/registerOBU2v03/registerOBU2_GetCar", ReplyAction="registerOBU2/registerOBU2v03/registerOBU2_GetCarResponse")]
        [System.ServiceModel.FaultContractAttribute(typeof(System.ServiceModel.FaultException), Action="registerOBU2/registerOBU2v03/registerOBU2_GetCarFaultExceptionFault", Name="FaultException", Namespace="http://schemas.datacontract.org/2004/07/System.ServiceModel")]
        Logic.OBU2.Vehicle registerOBU2_GetCar(Logic.OBU2.VhodniParametri VP, string sifra_vozila);
        
        [System.ServiceModel.OperationContractAttribute(Action="registerOBU2/registerOBU2v03/registerOBU2_GetCar", ReplyAction="registerOBU2/registerOBU2v03/registerOBU2_GetCarResponse")]
        System.Threading.Tasks.Task<Logic.OBU2.Vehicle> registerOBU2_GetCarAsync(Logic.OBU2.VhodniParametri VP, string sifra_vozila);
        
        [System.ServiceModel.OperationContractAttribute(Action="registerOBU2/registerOBU2v03/registerOBU2_GetCarTypes", ReplyAction="registerOBU2/registerOBU2v03/registerOBU2_GetCarTypesResponse")]
        [System.ServiceModel.FaultContractAttribute(typeof(System.ServiceModel.FaultException), Action="registerOBU2/registerOBU2v03/registerOBU2_GetCarTypesFaultExceptionFault", Name="FaultException", Namespace="http://schemas.datacontract.org/2004/07/System.ServiceModel")]
        string[] registerOBU2_GetCarTypes(Logic.OBU2.VhodniParametri VP);
        
        [System.ServiceModel.OperationContractAttribute(Action="registerOBU2/registerOBU2v03/registerOBU2_GetCarTypes", ReplyAction="registerOBU2/registerOBU2v03/registerOBU2_GetCarTypesResponse")]
        System.Threading.Tasks.Task<string[]> registerOBU2_GetCarTypesAsync(Logic.OBU2.VhodniParametri VP);
        
        [System.ServiceModel.OperationContractAttribute(Action="registerOBU2/registerOBU2v03/registerOBU2_ChangePersonInCar", ReplyAction="registerOBU2/registerOBU2v03/registerOBU2_ChangePersonInCarResponse")]
        [System.ServiceModel.FaultContractAttribute(typeof(System.ServiceModel.FaultException), Action="registerOBU2/registerOBU2v03/registerOBU2_ChangePersonInCarFaultExceptionFault", Name="FaultException", Namespace="http://schemas.datacontract.org/2004/07/System.ServiceModel")]
        void registerOBU2_ChangePersonInCar(Logic.OBU2.VhodniParametri VP, string sifra_vozila, string trr_lastnika);
        
        [System.ServiceModel.OperationContractAttribute(Action="registerOBU2/registerOBU2v03/registerOBU2_ChangePersonInCar", ReplyAction="registerOBU2/registerOBU2v03/registerOBU2_ChangePersonInCarResponse")]
        System.Threading.Tasks.Task registerOBU2_ChangePersonInCarAsync(Logic.OBU2.VhodniParametri VP, string sifra_vozila, string trr_lastnika);
        
        [System.ServiceModel.OperationContractAttribute(Action="registerOBU2/registerOBU2v03/registerOBU2_FindWayAB", ReplyAction="registerOBU2/registerOBU2v03/registerOBU2_FindWayABResponse")]
        [System.ServiceModel.FaultContractAttribute(typeof(System.ServiceModel.FaultException), Action="registerOBU2/registerOBU2v03/registerOBU2_FindWayABFaultExceptionFault", Name="FaultException", Namespace="http://schemas.datacontract.org/2004/07/System.ServiceModel")]
        string[] registerOBU2_FindWayAB(Logic.OBU2.VhodniParametri VP, string tockaA, string tockaB, string tip_vozilap);
        
        [System.ServiceModel.OperationContractAttribute(Action="registerOBU2/registerOBU2v03/registerOBU2_FindWayAB", ReplyAction="registerOBU2/registerOBU2v03/registerOBU2_FindWayABResponse")]
        System.Threading.Tasks.Task<string[]> registerOBU2_FindWayABAsync(Logic.OBU2.VhodniParametri VP, string tockaA, string tockaB, string tip_vozilap);
        
        [System.ServiceModel.OperationContractAttribute(Action="registerOBU2/registerOBU2v03/registerOBU2_FindWayToA", ReplyAction="registerOBU2/registerOBU2v03/registerOBU2_FindWayToAResponse")]
        [System.ServiceModel.FaultContractAttribute(typeof(System.ServiceModel.FaultException), Action="registerOBU2/registerOBU2v03/registerOBU2_FindWayToAFaultExceptionFault", Name="FaultException", Namespace="http://schemas.datacontract.org/2004/07/System.ServiceModel")]
        string[] registerOBU2_FindWayToA(Logic.OBU2.VhodniParametri VP, string tockaA, string tip_vozilap);
        
        [System.ServiceModel.OperationContractAttribute(Action="registerOBU2/registerOBU2v03/registerOBU2_FindWayToA", ReplyAction="registerOBU2/registerOBU2v03/registerOBU2_FindWayToAResponse")]
        System.Threading.Tasks.Task<string[]> registerOBU2_FindWayToAAsync(Logic.OBU2.VhodniParametri VP, string tockaA, string tip_vozilap);
        
        [System.ServiceModel.OperationContractAttribute(Action="registerOBU2/registerOBU2v03/registerOBU2_GetWays", ReplyAction="registerOBU2/registerOBU2v03/registerOBU2_GetWaysResponse")]
        [System.ServiceModel.FaultContractAttribute(typeof(System.ServiceModel.FaultException), Action="registerOBU2/registerOBU2v03/registerOBU2_GetWaysFaultExceptionFault", Name="FaultException", Namespace="http://schemas.datacontract.org/2004/07/System.ServiceModel")]
        string[] registerOBU2_GetWays(Logic.OBU2.VhodniParametri VP);
        
        [System.ServiceModel.OperationContractAttribute(Action="registerOBU2/registerOBU2v03/registerOBU2_GetWays", ReplyAction="registerOBU2/registerOBU2v03/registerOBU2_GetWaysResponse")]
        System.Threading.Tasks.Task<string[]> registerOBU2_GetWaysAsync(Logic.OBU2.VhodniParametri VP);
        
        [System.ServiceModel.OperationContractAttribute(Action="registerOBU2/registerOBU2v03/registerOBU2_GetMyKey", ReplyAction="registerOBU2/registerOBU2v03/registerOBU2_GetMyKeyResponse")]
        [System.ServiceModel.FaultContractAttribute(typeof(System.ServiceModel.FaultException), Action="registerOBU2/registerOBU2v03/registerOBU2_GetMyKeyFaultExceptionFault", Name="FaultException", Namespace="http://schemas.datacontract.org/2004/07/System.ServiceModel")]
        string registerOBU2_GetMyKey(string naziv_storitve, string geslo);
        
        [System.ServiceModel.OperationContractAttribute(Action="registerOBU2/registerOBU2v03/registerOBU2_GetMyKey", ReplyAction="registerOBU2/registerOBU2v03/registerOBU2_GetMyKeyResponse")]
        System.Threading.Tasks.Task<string> registerOBU2_GetMyKeyAsync(string naziv_storitve, string geslo);
        
        [System.ServiceModel.OperationContractAttribute(Action="registerOBU2/registerOBU2v03/registerOBU2_CheckPermision", ReplyAction="registerOBU2/registerOBU2v03/registerOBU2_CheckPermisionResponse")]
        [System.ServiceModel.FaultContractAttribute(typeof(System.ServiceModel.FaultException), Action="registerOBU2/registerOBU2v03/registerOBU2_CheckPermisionFaultExceptionFault", Name="FaultException", Namespace="http://schemas.datacontract.org/2004/07/System.ServiceModel")]
        bool registerOBU2_CheckPermision(Logic.OBU2.VhodniParametri VP, string client_key);
        
        [System.ServiceModel.OperationContractAttribute(Action="registerOBU2/registerOBU2v03/registerOBU2_CheckPermision", ReplyAction="registerOBU2/registerOBU2v03/registerOBU2_CheckPermisionResponse")]
        System.Threading.Tasks.Task<bool> registerOBU2_CheckPermisionAsync(Logic.OBU2.VhodniParametri VP, string client_key);
        
        [System.ServiceModel.OperationContractAttribute(Action="registerOBU2/registerOBU2v03/registerOBU2_GetAvailabeServices", ReplyAction="registerOBU2/registerOBU2v03/registerOBU2_GetAvailabeServicesResponse")]
        [System.ServiceModel.FaultContractAttribute(typeof(System.ServiceModel.FaultException), Action="registerOBU2/registerOBU2v03/registerOBU2_GetAvailabeServicesFaultExceptionFault", Name="FaultException", Namespace="http://schemas.datacontract.org/2004/07/System.ServiceModel")]
        string[] registerOBU2_GetAvailabeServices(Logic.OBU2.VhodniParametri VP);
        
        [System.ServiceModel.OperationContractAttribute(Action="registerOBU2/registerOBU2v03/registerOBU2_GetAvailabeServices", ReplyAction="registerOBU2/registerOBU2v03/registerOBU2_GetAvailabeServicesResponse")]
        System.Threading.Tasks.Task<string[]> registerOBU2_GetAvailabeServicesAsync(Logic.OBU2.VhodniParametri VP);
        
        [System.ServiceModel.OperationContractAttribute(Action="registerOBU2/registerOBU2v03/registerOBU2_AddServicePermision", ReplyAction="registerOBU2/registerOBU2v03/registerOBU2_AddServicePermisionResponse")]
        [System.ServiceModel.FaultContractAttribute(typeof(System.ServiceModel.FaultException), Action="registerOBU2/registerOBU2v03/registerOBU2_AddServicePermisionFaultExceptionFault", Name="FaultException", Namespace="http://schemas.datacontract.org/2004/07/System.ServiceModel")]
        void registerOBU2_AddServicePermision(Logic.OBU2.VhodniParametri VP, string client_key);
        
        [System.ServiceModel.OperationContractAttribute(Action="registerOBU2/registerOBU2v03/registerOBU2_AddServicePermision", ReplyAction="registerOBU2/registerOBU2v03/registerOBU2_AddServicePermisionResponse")]
        System.Threading.Tasks.Task registerOBU2_AddServicePermisionAsync(Logic.OBU2.VhodniParametri VP, string client_key);
    }
    
    [System.CodeDom.Compiler.GeneratedCodeAttribute("System.ServiceModel", "4.0.0.0")]
    public interface registerOBU2v03Channel : Logic.OBU2.registerOBU2v03, System.ServiceModel.IClientChannel {
    }
    
    [System.Diagnostics.DebuggerStepThroughAttribute()]
    [System.CodeDom.Compiler.GeneratedCodeAttribute("System.ServiceModel", "4.0.0.0")]
    public partial class registerOBU2v03Client : System.ServiceModel.ClientBase<Logic.OBU2.registerOBU2v03>, Logic.OBU2.registerOBU2v03 {
        
        public registerOBU2v03Client() {
        }
        
        public registerOBU2v03Client(string endpointConfigurationName) : 
                base(endpointConfigurationName) {
        }
        
        public registerOBU2v03Client(string endpointConfigurationName, string remoteAddress) : 
                base(endpointConfigurationName, remoteAddress) {
        }
        
        public registerOBU2v03Client(string endpointConfigurationName, System.ServiceModel.EndpointAddress remoteAddress) : 
                base(endpointConfigurationName, remoteAddress) {
        }
        
        public registerOBU2v03Client(System.ServiceModel.Channels.Binding binding, System.ServiceModel.EndpointAddress remoteAddress) : 
                base(binding, remoteAddress) {
        }
        
        public string registerOBU2_AddCar(Logic.OBU2.VhodniParametri VP, string tip_Vozila, string ID_pot, string trr_lastnika) {
            return base.Channel.registerOBU2_AddCar(VP, tip_Vozila, ID_pot, trr_lastnika);
        }
        
        public System.Threading.Tasks.Task<string> registerOBU2_AddCarAsync(Logic.OBU2.VhodniParametri VP, string tip_Vozila, string ID_pot, string trr_lastnika) {
            return base.Channel.registerOBU2_AddCarAsync(VP, tip_Vozila, ID_pot, trr_lastnika);
        }
        
        public void registerOBU2_RemoveObsoleteCar(Logic.OBU2.VhodniParametri VP, string sifra_vozila) {
            base.Channel.registerOBU2_RemoveObsoleteCar(VP, sifra_vozila);
        }
        
        public System.Threading.Tasks.Task registerOBU2_RemoveObsoleteCarAsync(Logic.OBU2.VhodniParametri VP, string sifra_vozila) {
            return base.Channel.registerOBU2_RemoveObsoleteCarAsync(VP, sifra_vozila);
        }
        
        public bool registerOBU2_CheckCarExsistance(Logic.OBU2.VhodniParametri VP, string sifra_vozila) {
            return base.Channel.registerOBU2_CheckCarExsistance(VP, sifra_vozila);
        }
        
        public System.Threading.Tasks.Task<bool> registerOBU2_CheckCarExsistanceAsync(Logic.OBU2.VhodniParametri VP, string sifra_vozila) {
            return base.Channel.registerOBU2_CheckCarExsistanceAsync(VP, sifra_vozila);
        }
        
        public string registerOBU2_GetCarEdge(Logic.OBU2.VhodniParametri VP, string sifra_vozila) {
            return base.Channel.registerOBU2_GetCarEdge(VP, sifra_vozila);
        }
        
        public System.Threading.Tasks.Task<string> registerOBU2_GetCarEdgeAsync(Logic.OBU2.VhodniParametri VP, string sifra_vozila) {
            return base.Channel.registerOBU2_GetCarEdgeAsync(VP, sifra_vozila);
        }
        
        public string registerOBU2_GetPersonInCar(Logic.OBU2.VhodniParametri VP, string sifra_vozila) {
            return base.Channel.registerOBU2_GetPersonInCar(VP, sifra_vozila);
        }
        
        public System.Threading.Tasks.Task<string> registerOBU2_GetPersonInCarAsync(Logic.OBU2.VhodniParametri VP, string sifra_vozila) {
            return base.Channel.registerOBU2_GetPersonInCarAsync(VP, sifra_vozila);
        }
        
        public Logic.OBU2.Vehicle registerOBU2_GetCar(Logic.OBU2.VhodniParametri VP, string sifra_vozila) {
            return base.Channel.registerOBU2_GetCar(VP, sifra_vozila);
        }
        
        public System.Threading.Tasks.Task<Logic.OBU2.Vehicle> registerOBU2_GetCarAsync(Logic.OBU2.VhodniParametri VP, string sifra_vozila) {
            return base.Channel.registerOBU2_GetCarAsync(VP, sifra_vozila);
        }
        
        public string[] registerOBU2_GetCarTypes(Logic.OBU2.VhodniParametri VP) {
            return base.Channel.registerOBU2_GetCarTypes(VP);
        }
        
        public System.Threading.Tasks.Task<string[]> registerOBU2_GetCarTypesAsync(Logic.OBU2.VhodniParametri VP) {
            return base.Channel.registerOBU2_GetCarTypesAsync(VP);
        }
        
        public void registerOBU2_ChangePersonInCar(Logic.OBU2.VhodniParametri VP, string sifra_vozila, string trr_lastnika) {
            base.Channel.registerOBU2_ChangePersonInCar(VP, sifra_vozila, trr_lastnika);
        }
        
        public System.Threading.Tasks.Task registerOBU2_ChangePersonInCarAsync(Logic.OBU2.VhodniParametri VP, string sifra_vozila, string trr_lastnika) {
            return base.Channel.registerOBU2_ChangePersonInCarAsync(VP, sifra_vozila, trr_lastnika);
        }
        
        public string[] registerOBU2_FindWayAB(Logic.OBU2.VhodniParametri VP, string tockaA, string tockaB, string tip_vozilap) {
            return base.Channel.registerOBU2_FindWayAB(VP, tockaA, tockaB, tip_vozilap);
        }
        
        public System.Threading.Tasks.Task<string[]> registerOBU2_FindWayABAsync(Logic.OBU2.VhodniParametri VP, string tockaA, string tockaB, string tip_vozilap) {
            return base.Channel.registerOBU2_FindWayABAsync(VP, tockaA, tockaB, tip_vozilap);
        }
        
        public string[] registerOBU2_FindWayToA(Logic.OBU2.VhodniParametri VP, string tockaA, string tip_vozilap) {
            return base.Channel.registerOBU2_FindWayToA(VP, tockaA, tip_vozilap);
        }
        
        public System.Threading.Tasks.Task<string[]> registerOBU2_FindWayToAAsync(Logic.OBU2.VhodniParametri VP, string tockaA, string tip_vozilap) {
            return base.Channel.registerOBU2_FindWayToAAsync(VP, tockaA, tip_vozilap);
        }
        
        public string[] registerOBU2_GetWays(Logic.OBU2.VhodniParametri VP) {
            return base.Channel.registerOBU2_GetWays(VP);
        }
        
        public System.Threading.Tasks.Task<string[]> registerOBU2_GetWaysAsync(Logic.OBU2.VhodniParametri VP) {
            return base.Channel.registerOBU2_GetWaysAsync(VP);
        }
        
        public string registerOBU2_GetMyKey(string naziv_storitve, string geslo) {
            return base.Channel.registerOBU2_GetMyKey(naziv_storitve, geslo);
        }
        
        public System.Threading.Tasks.Task<string> registerOBU2_GetMyKeyAsync(string naziv_storitve, string geslo) {
            return base.Channel.registerOBU2_GetMyKeyAsync(naziv_storitve, geslo);
        }
        
        public bool registerOBU2_CheckPermision(Logic.OBU2.VhodniParametri VP, string client_key) {
            return base.Channel.registerOBU2_CheckPermision(VP, client_key);
        }
        
        public System.Threading.Tasks.Task<bool> registerOBU2_CheckPermisionAsync(Logic.OBU2.VhodniParametri VP, string client_key) {
            return base.Channel.registerOBU2_CheckPermisionAsync(VP, client_key);
        }
        
        public string[] registerOBU2_GetAvailabeServices(Logic.OBU2.VhodniParametri VP) {
            return base.Channel.registerOBU2_GetAvailabeServices(VP);
        }
        
        public System.Threading.Tasks.Task<string[]> registerOBU2_GetAvailabeServicesAsync(Logic.OBU2.VhodniParametri VP) {
            return base.Channel.registerOBU2_GetAvailabeServicesAsync(VP);
        }
        
        public void registerOBU2_AddServicePermision(Logic.OBU2.VhodniParametri VP, string client_key) {
            base.Channel.registerOBU2_AddServicePermision(VP, client_key);
        }
        
        public System.Threading.Tasks.Task registerOBU2_AddServicePermisionAsync(Logic.OBU2.VhodniParametri VP, string client_key) {
            return base.Channel.registerOBU2_AddServicePermisionAsync(VP, client_key);
        }
    }
}
