﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.ServiceModel;
using System.ServiceModel.Channels;
using System.Text;
using System.Threading.Tasks;

namespace ShareACar
{
    public class BLL
    {


        public static void createCar(Car car)
        {
            Logic.OBU2.registerOBU2v03Client client = new Logic.OBU2.registerOBU2v03Client();
            Logic.OBU2.VhodniParametri vp = new Logic.OBU2.VhodniParametri();
            traas.ServiceImplClient traas = new traas.ServiceImplClient();
            traas.Endpoint.Address = new EndpointAddress("http://164.8.251.96:8088/TRAAS_WS");

            try
            {
                vp.PotDoTraasa = "http://164.8.251.96:8088/TRAAS_WS";
                vp.Key = client.registerOBU2_GetMyKey("shareAcar2", "zamenjajMe");
                string[] poti = client.registerOBU2_GetWays(vp);
                string[] tipiVozil = client.registerOBU2_GetCarTypes(vp);
                var imeAvta = client.registerOBU2_AddCar(vp, tipiVozil[0], poti[3], "trr");
                string[] idList = traas.Vehicle_getIDList();
                if (idList.Contains(imeAvta)) {
                    traas.Vehicle_setSpeed(imeAvta, 0);
                }


                car.setVehicleId(imeAvta);
                Database.addCarToDatabase(car);
                string[] idList2 = traas.Vehicle_getIDList();
                var ime = traas.Vehicle_getVehicleClass(imeAvta);
                string[] idList3 = traas.Vehicle_getIDList();
            }
            catch (FaultException fex)
            {
                throw fex;
            }
        }

        public static Car getCar(string vehicleId)
        {
            return Database.getCarById(vehicleId);
        }

        public static Car[] getCars()
        {
            return Database.getCars();
        }

        public static void updateCarName(string vehicleId, string name)
        {
            Database.updateCarById(vehicleId, name);

        }
        public static void removeCar(string vehicleId)
        {
            Database.removeCarById(vehicleId);
        }
        public static Location getCarLocation(string vehicleId, string trassIpAddress)
        {
            traas.ServiceImplClient traas = new traas.ServiceImplClient();
            if (!string.IsNullOrEmpty(trassIpAddress))
            {
                traas.Endpoint.Address = new EndpointAddress(trassIpAddress);
            }
            string[] idList = traas.Vehicle_getIDList();
            foreach (string s in idList)
            {
                if (s == vehicleId)
                {
                    traas.sumoPosition2D a = traas.Vehicle_getPosition(vehicleId);
                    return new Location(a.x, a.y);
                }
            }
            throw new Exception("ERROR: No location found!");
        }
        public static Car[] findClosestCars(double range, double x, double y, string trassIpAddress)
        {
            traas.ServiceImplClient traas = new traas.ServiceImplClient();
            if (!string.IsNullOrEmpty(trassIpAddress))
            {
                traas.Endpoint.Address = new EndpointAddress(trassIpAddress);
            }

            Car[] carList = Database.getCars();
            List<Car> closestCars = new List<Car>();
            string[] idList = traas.Vehicle_getIDList();
            foreach (Car c in carList)
            {
                foreach (string s in idList)
                {
                    if (s == c.getVehicleId())
                    {
                        traas.sumoPosition2D vehPos = traas.Vehicle_getPosition(c.getVehicleId());
                        double distance = Math.Sqrt(Math.Pow((vehPos.x - x), 2) + Math.Pow((vehPos.y - y), 2));
                        if (distance < range)
                        {
                            closestCars.Add(c);
                        }
                    }
                }
               
            }

            return closestCars.ToArray();
        }
        public static void notifyImproperUse(string traasIpAddr, string vehicleId)
        {
            Logic.OBU1.obu1v3PortTypeClient obu1 = new Logic.OBU1.obu1v3PortTypeClient();
            string qName = obu1.OBU1_StartPublishingSpeed(60, traasIpAddr, vehicleId);
            Subscriber sub = new Subscriber();
            sub.GetMessages(qName, vehicleId);
        }
        public static void carsNearBy(double range, Location location, string ipAddr)
        {
            TopicPublisher publisher = new TopicPublisher();
            int timer = 0;
            while (timer < 60)
            {
                List<Car> cars = findClosestCars(range, location.getLatitude(), location.getLongitude(), ipAddr).ToList();
                foreach (Car c in cars)
                {
                    publisher.PublishMessage("Car nearBy: " + c.getName() + " with ID: " + c.getVehicleId() + "\n", "share1CarNearBy");
                }
                System.Threading.Thread.Sleep(5000);
                timer += 1;
            }

        }

        private static double checkCarSpeed(string traasIpAddr, string vehicleId)
        {
            traas.ServiceImplClient traas = new traas.ServiceImplClient();
            if (!string.IsNullOrEmpty(traasIpAddr))
            {
                traas.Endpoint.Address = new EndpointAddress(traasIpAddr);
            }
            string[] idList = traas.Vehicle_getIDList();
            foreach (string id in idList)
            {
                if (id == vehicleId)
                {
                    return traas.Vehicle_getSpeed(vehicleId);
                }
            }
            throw new Exception("There is no car with that id.");
        }
        public static void stopCar(string vehicleId)
        {
            traas.ServiceImplClient traas = new traas.ServiceImplClient();
            traas.Vehicle_setSpeed(vehicleId, 0);
        }

        public static void startSharingSession(string vehicleId, int trr, string traasIpAddr)
        {
            traas.ServiceImplClient traas = new traas.ServiceImplClient();
            if (!string.IsNullOrEmpty(traasIpAddr))
            {
                traas.Endpoint.Address = new EndpointAddress(traasIpAddr);
            }
            string[] idList = traas.Vehicle_getIDList();
            if (idList.Contains(vehicleId))
            {
                traas.Vehicle_setSpeed(vehicleId, 45);
                Database.createSession(vehicleId, trr);
            }
            else {
                throw new Exception("This car doesn't exists in simulation.");
            }
        }

        public static void stopSharingSession(int sessionId, string traasIpAddr)
        {
            Session seja = Database.getSessionById(sessionId);
            traas.ServiceImplClient traas = new traas.ServiceImplClient();
            if (!string.IsNullOrEmpty(traasIpAddr))
            {
                traas.Endpoint.Address = new EndpointAddress(traasIpAddr);
            }
            string[] idList = traas.Vehicle_getIDList();
            if (idList.Contains(seja.getVehicleId()))
            {
                double speed = traas.Vehicle_getSpeed(seja.getVehicleId());
                if (speed == 0)
                {
                    throw new Exception("This car is not driving currently!");
                }
                else
                {
                    traas.Vehicle_setSpeed(seja.getVehicleId(), 0);
                    Database.updateSession(sessionId);
                }
            }
            else {
                throw new Exception("This car doesn't exists in simulation.");
            }
        }
        public static Session[] getSessions()
        {
            return Database.getSessions();
        }
        public static Session getSessionById(string id)
        {
            return Database.getSessionById(int.Parse(id));
        }
        public static int getSessionId(string vehicleId, int trr)
        {
            return Database.getSessionId(vehicleId, trr);
        }
        public static void removeSessionById(string id)
        {
            Database.removeSessionById(int.Parse(id));
        }
        private double calculateSessionCost(Session s)
        {
            TimeSpan time = s.getEndTime().Subtract(s.getStartTime());
            return time.TotalMinutes * 0.20;
        }


        public static void createCustomer(Customer cust)
        {
            Database.addCustomerToDatabase(cust);
        }
        public static Customer getCustomer(int trr)
        {
            return Database.getCustomerByTrr(trr);
        }
        public static Customer[] getCustomers()
        {
            return Database.getCustomers();
        }
        public static void banCustomer(int trr, int days)
        {
            Customer customer = Database.getCustomerByTrr(trr);
            DateTime banDateEnd = DateTime.Now;
            banDateEnd.AddDays(days);
            customer.setBanDateEnd(banDateEnd);
            Database.banCustomer(trr);

            //BankService.CreateBill(int trr, double cost);
        }
        public static void removeCustomer(int trr)
        {
            Database.removeCustomerByTrr(trr);
        }

        public static Session getSessionByVehIdTrr(string vehicleId, int trr)
        {
            return Database.getSessionByVehIdTrr(vehicleId, trr);
        }

    }
}
