﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Runtime.Serialization;

namespace ShareACar
{
    [DataContract]
    public class Location
    {
        [DataMember]
        double latitude = 0;
        [DataMember]
        double longitude = 0;

        public Location(double latitude, double longitude)
        {
            this.longitude = longitude;
            this.latitude = latitude;
        }

        public double getLatitude()
        {
            return latitude;
        }

        public double getLongitude()
        {
            return longitude;
        }

    }
}
