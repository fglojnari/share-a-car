﻿using RabbitMQ.Client;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ShareACar
{
    public class TopicPublisher
    {
        public void PublishMessage(string message, string exchangeName = "Share1Speeding")
        {
            ConnectionFactory factory = new ConnectionFactory();
            IProtocol protocol = Protocols.AMQP_0_9_1;
            factory.VirtualHost = "/";
            factory.UserName = "soa";
            factory.Password = "soasoa";
            factory.HostName = "164.8.251.96";
            factory.Port = 5672;
            factory.Protocol = protocol;
            using (IConnection conn = factory.CreateConnection())
            {
                using (IModel ch = conn.CreateModel())
                {
                    ch.ExchangeDeclare(exchangeName, "fanout", true);
                    IBasicProperties basicProperties = ch.CreateBasicProperties();
                    basicProperties.Persistent = true;
                    basicProperties.MessageId = "ID:" + System.Guid.NewGuid().ToString();
                    basicProperties.ContentType = "text/plain";
                    ch.BasicPublish(exchangeName, "", basicProperties, Encoding.UTF8.GetBytes(message));

                }
            }
        }
    }
}
