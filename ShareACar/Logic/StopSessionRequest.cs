﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.Text;
using System.Threading.Tasks;

namespace ShareACar
{
    [DataContract]
    public class StopSessionRequest
    {
        [DataMember]
        private int sessionId;
        [DataMember]
        private string traasIpAddr;
        public StopSessionRequest(int sessionId, string ipAddr)
        {
            this.sessionId = sessionId;
            this.traasIpAddr = ipAddr;
        }
        public int getSessionId()
        {
            return sessionId;
        }
        public string getTraasIpAddr()
        {
            return traasIpAddr;
        }
    }
}
