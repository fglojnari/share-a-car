﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Runtime.Serialization;

namespace ShareACar
{
    [DataContract]
    public class CreateSessionRequest
    {
        [DataMember]
        private string vehicleId;
        [DataMember]
        private int trr;
        [DataMember]
        private string traasIpAddr;

        public CreateSessionRequest(string vehicleId, int trr, string ipAddr)
        {
            this.vehicleId = vehicleId;
            this.trr = trr;
            this.traasIpAddr = ipAddr;
        }

        public string getVehicleId()
        {
            return vehicleId;
        }

        public int getTrr()
        {
            return trr;
        }
        public string getTraasIpAddr()
        {
            return traasIpAddr;
        }
    }
}
