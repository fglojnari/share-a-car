# share-a-car

Simulating "share a car" service, with drivers leaving cars on the parking lot, and paying the price of using the car per minute. We set REST and SOAP services for controlling the share-a-car service. We also used REST and SOAP services from SUMO (Simulation of Urban MObility) to control vehicle motion. Project is written in C#, WCF project.
